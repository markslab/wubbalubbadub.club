import { useRef } from 'react'
import { useInView } from 'react-intersection-observer'
import useSWRInfinite from 'swr/infinite'
import fetcher from '../lib/fetcher'
import { Items } from '../lib/types'
import Track from './Track'

export default function PlaylistTracks(props) {
  const PAGE_SIZE = 20
  const  { data, error, isValidating, mutate, size, setSize } = useSWRInfinite<Items>(
      (index) => `/api/playlist/${props.id}/tracks?offset=${index*PAGE_SIZE}&limit=${PAGE_SIZE}`,
      fetcher
    )

  const tracks = data ? data.flatMap( d => d.items) : []
  const total = data ? data[0].total : 0

  const butt = () => {
    return (
      <button
        disabled={(tracks.length == total)}
        onClick={() => setSize(size + 1)}
      >
        {(tracks.length == total)
          ? "no more"
          : `load ${(PAGE_SIZE > total-tracks.length) ? total-tracks.length : PAGE_SIZE } more`} tracks
      </button>
    )
  }

  const autoLoad = () => {
    const { ref, inView, entry } = useInView({ threshold: 0 })

    if ( inView && (PAGE_SIZE*size < total) && !isValidating ) {
      setSize( size+1 )
    }

    return (
      <p
        className="load-more"
        ref={ref}
      />
    )

  }

  if (props.style == 'list') {
    return (
      <ol className="playlist-tracks">
      { tracks.map((item) =>
          <li key={item.track.id}><Track track={item.track} style="list" /></li>
      )}
      </ol>
    )
  } else {
    var i = 1
    const cols = ['name/album/artist','length','popularity','danceability','energy','key','tempo','time signature','valence']
    return (
      <div>
        {butt()}
        <table>
          <thead>
            <tr>
              <th scope="column"> # </th>
              { cols.map((heading) =>
                <th scope="column"> {heading} </th>
              )}
            </tr>
          </thead>
          { tracks.map((item) =>
              <tr key={item.track.id}>
                <td>{i++}</td>
                <Track track={item.track} style="table" cols={cols} />
              </tr>
          )}
        </table>
        {autoLoad()}
      </div>
    )
  }
}
