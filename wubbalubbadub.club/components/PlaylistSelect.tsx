import { useState, useRef } from 'react';

import Playlist from './Playlist'

export default function PlaylistSelect (props) {
  const selectEl = useRef(null);
  const [select, setSelect] = useState({ playlist: {} });
  const playlists = props.owner ? props.playlists.filter( list => list.owner.display_name === props.owner) : props.playlists
  const playlistOptions = playlists.map( (playlist) =>
    <option key={playlist.id} value={playlist.id}>
      {playlist.name}
    </option>
  )
  const selectPlaylist = () => {
    const playlistId = selectEl.current.value;
    const playlist = playlists.find( list => list.id === playlistId )
    setSelect({playlist: playlist})
  }

  return (
    <div>
      <label for="playlist-select">
        Choose a playlist:
        <select
          name="playlists"
          id="playlist-select"
          ref={selectEl}
          onChange={selectPlaylist}>
            { playlistOptions }
        </select>
      </label>
      <Playlist playlist={select.playlist} />
    </div>
  );

}
