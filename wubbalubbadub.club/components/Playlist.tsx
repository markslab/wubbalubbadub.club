import Image from 'next/image'
import PlaylistTracks from './PlaylistTracks'

export default function Playlist(props) {
  const img = props.playlist.images?.pop()

  return (
    <div className="playlist container">
      <div className="playlist-meta">
        { img &&
          <Image src={img.url} alt="album image" width={img.width} height={img.height} />
        }
        <a
          className=""
          href={props.playlist.href}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.playlist.name}
        </a>
        <p>
          {props.playlist.description}
        </p>
      </div>
      { props.playlist.id &&
        <PlaylistTracks id={props.playlist.id} />
      }
    </div>
  );
}
