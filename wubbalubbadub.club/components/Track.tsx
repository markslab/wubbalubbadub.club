import useSWR from 'swr'
import Image from 'next/image'
import fetcher from '../lib/fetcher'
import { TrackFeatures } from '../lib/types'
import styles from '../styles/Track.module.css'

export default function Track(props) {
  const endpoint = `/api/track/${props.track.id}/features`
  const { data, error } = useSWR<TrackFeatures>(endpoint, fetcher);
  const getLength = function (ms) {
    const min = Math.floor(ms / 60000)
    const sec = ((ms % 60000) / 1000).toFixed(0)
    return min + ":" + (sec < 10 ? '0' : '') + sec
  }
  const colRender = function (heading) {
    switch (heading) {

      case 'name/album/artist':
        const img = props.track.album?.images?.pop()
        return (
          <div className={styles.grid}>
            <div>
              { img &&
                <Image src={img.url} alt="album image" width={80} height={80} />
              }
            </div>
            <div>
              <p>
                <a
                  className="track-name"
                  href={props.track.href}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {props.track.name}
                </a>
                <br />
                <a
                  className="album-name"
                  href={props.track.album.href}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                {props.track.album.name}
                </a>
                <br />
                <a
                  className="artist-name"
                  href={props.track.artists[0].href}
                  target="_blank"
                  rel="noopener noreferrer"
                  >
                    {props.track.artists[0].name}
                </a>
              </p>
            </div>
          </div>
        )
        break;

      case 'name':
        return (
          <a
            className="track-name"
            href={props.track.href}
            target="_blank"
            rel="noopener noreferrer"
          >
            {props.track.name}
          </a>
        )
        break;

      case 'album':
        return (
          <a
            className="album-name"
            href={props.track.album.href}
            target="_blank"
            rel="noopener noreferrer"
          >
          {props.track.album.name}
          </a>
        )
        break;

      case 'artist':
        return (
          <a
            className="artist-name"
            href={props.track.artists[0].href}
            target="_blank"
            rel="noopener noreferrer"
            >
              {props.track.artists[0].name}
          </a>
        )
        break;

      case 'length':
        return (
          <span>
            {getLength(props.track.duration_ms)}
          </span>
        )
        break;
      case 'popularity':
        return (
          <span>
            {props.track.popularity}
          </span>
        )
        break;
      case 'danceability':
        return (
          <span>
            { data &&
              data.danceability
            }
          </span>
        )
        break;
      case 'energy':
        return (
          <span>
          { data &&
            data.energy
          }
          </span>
        )
        break;
      case 'key':
        return (
          <span>
          { data &&
            data.key
          }
          </span>
        )
        break;

      case 'tempo':
        return (
          <span>
          { data &&
            data.tempo
          }
          </span>
        )
        break;
      case 'time signature':
        return (
          <span>
          { data &&
            data.time_signature
          }
          </span>
        )
        break;
      case 'valence':
        return (
          <span>
          { data &&
            data.valence
          }
          </span>
        )
        break;
    }
  }

  if (props.style == 'table') {
    return (
        props.cols.map( (heading) =>
          <td> {colRender(heading)} </td>
        )
    )
  } else {
    return (
      <div className="track container">
        <a
          className="track-name"
          href={props.track.href}
          target="_blank"
          rel="noopener noreferrer"
        >
          {props.track.name}
        </a>
        &nbsp;by&nbsp;
        <a
            className="artist-name"
            href={props.track.artists[0].href}
            target="_blank"
            rel="noopener noreferrer"
          >
            {props.track.artists[0].name}
        </a>
        &nbsp;from&nbsp;
        <a
          className="album-name"
          href={props.track.album.href}
          target="_blank"
          rel="noopener noreferrer"
        >
        {props.track.album.name}
        </a>
      </div>
    );
  }

}
