import type { NextPage } from 'next'
import useSWR from 'swr'
import Head from 'next/head'
import Image from 'next/image'
import PlaylistSelect from '../components/PlaylistSelect'
import styles from '../styles/Home.module.css'

import fetcher from 'lib/fetcher';
import { Playlists } from 'lib/types';

const Blaaarg: NextPage = () => {
  const { data, error } = useSWR<Playlists>('/api/playlists', fetcher);

  return (
    <div className={styles.container}>
      <Head>
        <title>Wubba Lubba Dub Club</title>
        <meta name="description" content="Build Spotify playlists with the full power of the Spotify Web API." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          WUBBALUBBADUBCLUB<a href="https://youtu.be/mJlmxYRrSLk" target="_blank">!</a>
        </h1>
        <h2>
          Let's make a playlist.
        </h2>

        { error &&
          <p>Error</p>
        }
        { data &&
          <PlaylistSelect playlists={data.items} owner="blaaarg"  />
        }


      </main>

      <footer className={styles.footer}>

      </footer>
    </div>
  )
}

export default Blaaarg
