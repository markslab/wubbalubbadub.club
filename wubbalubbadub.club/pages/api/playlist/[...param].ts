import { type NextRequest } from 'next/server'
import { getPlaylistTracks } from '../../../lib/spotify'

export default async (_, res) => {
  const { param, offset, limit } = _.query
  const pid = param[0]
  const action = param[1]

  switch (action) {

    case 'tracks':
      const response = await getPlaylistTracks(pid, offset, limit)
      const {href, items, next, previous, total} = await response.json()
      return res.status(200).json({href, items, next, previous, total})
      break;
  }


};
