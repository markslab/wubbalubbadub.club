import { type NextRequest } from 'next/server'
import { getTrackFeatures } from '../../../lib/spotify'

export default async (_, res) => {
  const { param } = _.query
  const tid = param[0]
  const action = param[1]

  switch (action) {
    default:
      const response = await getTrackFeatures(tid)
      const {acousticness, analysis_url, danceability, duration_ms, energy, id, instrumentalness, key, liveness, loudness, mode, speechiness, tempo, time_signature, track_href, type, uri, valence} = await response.json()
      return res.status(200).json({acousticness, analysis_url, danceability, duration_ms, energy, id, instrumentalness, key, liveness, loudness, mode, speechiness, tempo, time_signature, track_href, type, uri, valence})
      break;
  }

};
