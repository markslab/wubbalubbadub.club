import { type NextRequest } from 'next/server'
import { getPlaylists } from '../../lib/spotify'

export default async (_, res) => {

  const response = await getPlaylists()
  const {href, items, limit, next, offset, previous, total} = await response.json()

  return res.status(200).json({href, items, limit, next, offset, previous, total})

};
