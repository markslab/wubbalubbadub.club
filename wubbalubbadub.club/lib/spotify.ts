import querystring from 'querystring';

const client_id = process.env.SPOTIFY_CLIENT_ID;
const client_secret = process.env.SPOTIFY_CLIENT_SECRET;
const refresh_token = process.env.SPOTIFY_REFRESH_TOKEN;

const basic = Buffer.from(`${client_id}:${client_secret}`).toString('base64');

const TOKEN_ENDPOINT = `https://accounts.spotify.com/api/token`;
const PLAYLISTS_ENDPOINT = `https://api.spotify.com/v1/me/playlists`;
const TRACKS_ENDPOINT = 'https://api.spotify.com/v1/playlists'
const FEATURES_ENDPOINT = 'https://api.spotify.com/v1/audio-features'


const getAccessToken = async () => {
  const response = await fetch(TOKEN_ENDPOINT, {
    method: 'POST',
    headers: {
      Authorization: `Basic ${basic}`,
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: new URLSearchParams({
      grant_type: 'refresh_token',
      refresh_token
    })
  })

  return response.json()
}

const fetchEndpoint = async (endpoint) => {
  const { access_token } = await getAccessToken();

  return fetch(endpoint, {
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
}

const fetchAllItems = async (endpoint) => {
  const { access_token } = await getAccessToken()
  const response = await fetch(endpoint, {
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  })
  const { href, items, limit, next, offset, previous, total } = await response.json()

  return new Promise( (resolve, reject) => {
    if (!response.ok) reject(response.status)
    if (items.length == total) resolve( items )
    else {
      var allItems = items,
        nextItems = [],
        n = 1,
        more
      while ( limit*n < total) {
        nextItems.push(`${endpoint}&offset=${limit*n}`)
        n++
      }
      more = nextItems.map((ep) => fetch( ep, {headers:{Authorization: `Bearer ${access_token}`}} ) )
      Promise.all(more).then( (res) => {
        Promise.all( res.map( r => r.json()) ).then( (data) => {
          data.forEach( d => {
            allItems = allItems.concat(d.items)
          })
          resolve(allItems)
        })
      })
    }
  })
}

export const getPlaylists = async () => {
  const { access_token } = await getAccessToken();

  return fetch(PLAYLISTS_ENDPOINT, {
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export const getPlaylistTracks = async (id, offset, limit) => {
  const { access_token } = await getAccessToken();
  const endpoint = `${TRACKS_ENDPOINT}/${id}/tracks?fields=href,items(track(id,name,href,album(name,href,images,release_date),artists,duration_ms,popularity)),next,previous,total&offset=${offset}&limit=${limit}`

  return fetch(endpoint, {
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}

export const getTrackFeatures = async (id) => {
  const { access_token } = await getAccessToken();
  const endpoint = `${FEATURES_ENDPOINT}/${id}`

  return fetch(endpoint, {
    headers: {
      Authorization: `Bearer ${access_token}`
    }
  });
}
